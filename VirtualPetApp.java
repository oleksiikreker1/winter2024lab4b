import java.util.Scanner;

public class VirtualPetApp {
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Turtle[] bale = new Turtle[1];
		
		for(int i = 0; i < bale.length; i++){
			
			
			System.out.println("Is your turtle a land or sea turtle?");
			String habitat = reader.nextLine();
			//bale[i].setHabitat(habitat);
			
			System.out.println("How much does your turtle weigh?");
			double weight = Double.parseDouble(reader.nextLine());
			//bale[i].setWeight(weight);
			
			System.out.println("Is your turtle friends with Master Oogway? True or False");
			boolean isFriendsWithOogway = Boolean.parseBoolean(reader.nextLine());
			//bale[i].setIsFriendsWithOogway(isFriendsWithOogway);
			
			bale[i] = new Turtle(habitat, weight, isFriendsWithOogway);
		}
		
		System.out.println(bale[bale.length - 1].getHabitat());
		System.out.println(bale[bale.length - 1].getWeight());
		System.out.println(bale[bale.length - 1].getIsFriendsWithOogway());
		
		System.out.println("Is your turtle friends with Master Oogway? True or False");
		boolean isFriendsWithOogway = Boolean.parseBoolean(reader.nextLine());
		bale[bale.length - 1].setIsFriendsWithOogway(isFriendsWithOogway);
		
		System.out.println(bale[bale.length - 1].getHabitat());
		System.out.println(bale[bale.length - 1].getWeight());
		System.out.println(bale[bale.length - 1].getIsFriendsWithOogway());
	}
}