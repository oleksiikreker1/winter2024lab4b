public class Turtle {
	private String habitat;
	private double weight;
	private boolean isFriendsWithOogway;
	
	/*/Setter for habitat
	public void setHabitat(String habitat) {
		this.habitat = habitat;
	}
	
	//Setter for weight
	public void setWeight(double weight) {
		this.weight = weight;
	} */
	
	//Setter for isFriendsWithOogway
	public void setIsFriendsWithOogway(boolean isFriendsWithOogway) {
		this.isFriendsWithOogway = isFriendsWithOogway;
	}
	
	//Getter for habitat
	public String getHabitat() {
		return this.habitat;
	}
	
	//Getter for weight
	public double getWeight() {
		return this.weight;
	}
	
	//Getter for isFriendsWithOogway
	public boolean getIsFriendsWithOogway() {
		return this.isFriendsWithOogway;
	}
	
	//Constructor
	public Turtle(String habitat, double weight, boolean isFriendsWithOogway) {
		this.habitat = habitat;
		this.weight = weight;
		this.isFriendsWithOogway = isFriendsWithOogway;
	}
	
	public void eatSeaweed(){
		System.out.print("Your turle gained 6 pounds after eating 5 pieces of seaweed! It now weighs: " + (this.weight + 6));
	}
	
	public void sayHiToOogway() {
		if(this.isFriendsWithOogway == true) {
			System.out.println("Master Oogway says Hello Friend!");
		} else {
			System.out.println("Master Oogway is upset that you said you were not friends and he NEVER wants to talk again.");
		}
	}
}
